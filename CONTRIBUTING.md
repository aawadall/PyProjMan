# Welcome and Thank you
Hello and Thank you for dropping by

This project won't get anywhere without your contribution.
Be it reporting an issue, requesting a feature, designing or writing code, documentation, or helping with the graphics design.

This project is mostly written in Python, so some basic knowledge on how to code in Python will be required if you decide to contribute in code.
Anything else, will not require specific technical background.

## Contributions Needed
We need help in the following areas:

### Software Design
This project at its early stages, and we need to design classes, interfaces, methods, etc. to make it fly. You can have a look at the [documents folder][1] to get familiar with the project design, and what it is intended to do.
If you found something that needs change, please log an [issue][2] and label it **design**. Or you can submit a pull request with your proposed changes. I promise to review it ASAP.

### Development
This project is mostly written in Python. You can use [PyCharm][5] to do the required development.
There are two releases (defined as milestones) so far scheduled at [mid September][3] and [Early October][4] 2017, with issues (bugs, or features) assigned to each release. You can read the issue, figure how to implement or fix it, and submit a pull request with the proposed change.
Before you submit the change, make sure you execute and existing unit test for this feature, and if not present, please write that unit test. Thank you.

### Reporting Issues
You are welcome to play with the application, find bugs, or missing features. Please report any features or bugs in the [issue log][2], and assign it either **bug** or **feature** labels. The more issues we get, the richer the application would be.

### Graphics & UI Design
Currently, this application is entirely text based, but it should not remain this way. If you can propose UI ideas, icons, or even web page design, please contact me via email @ aawadall@ualberta.ca

### Documentation
Clean code requires proper documentation. Pull requests are welcome to different types of documentation:

* Classes and Application Design
* User Guides
* Developer Guides

Thank you 

[1]: https://github.com/aawadall/PyProjMan/tree/master/docs
[2]: https://github.com/aawadall/PyProjMan/issues
[3]: https://github.com/aawadall/PyProjMan/milestone/1
[4]: https://github.com/aawadall/PyProjMan/milestone/2
[5]: https://www.jetbrains.com/pycharm/download/
